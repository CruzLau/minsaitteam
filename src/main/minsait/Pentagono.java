
public class Pentagono{
    private double lado=0;
    private double apotema=0;
    private double perímetro;
    private double area;
    private double volumen;
    private double altura=0;

    public Pentagono(double lado,double apotema)
    {
        this.lado=lado;
        this.apotema=apotema;
        recalculaTodo();
    }

    public Pentagono(double lado,double apotema,double altura)
    {
        this.lado=lado;
        this.apotema=apotema;
        this.altura=altura;
        recalculaTodo();
    }

    public void setAltura(double altura)
    {
        this.altura=altura;
        recalculaTodo();
    }
    public void setLado(double lado)
    {
        this.lado=lado;
        recalculaTodo();
    }
    public void setApotema(double apotema){
        this.apotema=apotema;
        recalculaTodo();
    }

    public double getAltura(){return altura;}

    public double getVolumen(){return volumen;}
    public double getLado(){
        return lado;
    }
    public double getApotema(){
        return apotema;
    }
    public double getPerímetro(){
        return perímetro;
    }
    public double getArea(){
        return area;
    }
    public double calculaPerímetro(){
        perímetro=5*lado;
        return perímetro;
    }
    public double calculaVolumen()
    {
        volumen=area*altura;
        return volumen;
    }
    public double calculaArea(){
        area=perímetro*apotema/2;
        return area;
    }

    public void recalculaTodo()
    {
        calculaPerímetro();
        calculaArea();
        calculaVolumen();
    }

}

public class TrianguloEscaleno{
    private double area;
    public double calcularArea(double ladoA,double ladoB,double ladoC)
    {
        double semiperimetro;
        semiperimetro=(ladoA+ladoB+ladoC)/2;
        area=Math.sqrt(semiperimetro*(semiperimetro-ladoA)*(semiperimetro-ladoB)*(semiperimetro-ladoC));
        return area;
    }
    public double calcularPerimetro(double ladoA,double ladoB,double ladoC)
    {
        return ladoA+ladoB+ladoC;
    }

    public double calcularVolumen(double altura,double area)
    {
        return ((1/3)*(area)*altura);
    }
}


public class Rombo {
    private double diagonalMayor;
    private double diagonalMenor;
    private double lado;
    private double perimetroRombo;
    private double areaRombo;
    private  String volumenRombo;

    Rombo(double diagonalMayor, double diagonalMenor, double lado)
    {
        this.diagonalMayor = diagonalMayor;
        this.diagonalMenor = diagonalMenor;
        this.lado = lado;
    }
    public double getDiagonalMayor()
    {
        return diagonalMayor;
    }

    public void setDiagonalMayor(double diagonalMayor)
    {
        this.diagonalMayor = diagonalMayor;
    }

    public double getDiagonalMenor()
    {
        return diagonalMenor;
    }

    public void setDiagonalMenor(double diagonalMenor)
    {
        this.diagonalMenor = diagonalMenor;
    }
    public double getLado()
    {
        return lado;
    }
    public void setLado(double lado)
    {
        this.lado = lado;
    }
    public  double getPerimetro()
    {
        return perimetroRombo;
    }
    public void setPerimetroRombo()
    {
        this.perimetroRombo=perimetroRombo;
    }

    public double getAreaRombo()
    {
        return areaRombo;
    }
    public void  setAreaRombo()
    {
        this.areaRombo=areaRombo;
    }
    public String getVolumenRombo() {
        return volumenRombo;
    }
    public void setVolumenRombo()
    {
        this.volumenRombo=volumenRombo;
    }
    public double areaRombo()
    {
        return areaRombo = diagonalMayor * diagonalMenor/2;
    }
    public double perimetroRombo()
    {
        return perimetroRombo= lado* 4;
    }
    public void volumenRombo()
    {
        volumenRombo="Accion invalida";
    }

}

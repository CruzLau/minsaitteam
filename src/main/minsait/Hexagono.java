

public class Hexagono {
    int numeroLados=6;
    public double perimetroHexagono(double longitudLado){
        return numeroLados * longitudLado;
    }
    public double areaHexagonoPorApotema(double longitudLado,double apotema){
        return (perimetro(longitudLado) * apotema)/2;
    }
    public double areaHexagonoPorLongitudLado(double longitudLado){
        double angulo = Math.PI/6;
        double numerador = numeroLados * Math.pow(longitudLado,2);
        double denominador = 4*Math.tan(angulo);
        return numerador/denominador;
    }
    public double volumenHexagono(double longitudLado,double altura){
        return (areaHexagonoPorLongitudLado(longitudLado)*altura);
    }

    private double perimetro(double longitudLado){
        return numeroLados * longitudLado;
    }
}
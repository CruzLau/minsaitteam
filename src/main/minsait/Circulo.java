public class Circulo {


    public double pi = 3.1416;

    public double perimetroCirculo(double radio){
        double perimetro;
        perimetro = 2 * (pi*radio);
        return perimetro;
    }

    public double areaCirculo(double radio){
        double area;
        area = pi * (radio*radio);
        return area;
    }

    public double volumenCirculo(double radio, double altura){
        double volumen;
        volumen = (3/4) * pi * (radio * radio * radio);
        return volumen;
    }

}


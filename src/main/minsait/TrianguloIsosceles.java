
public class TrianguloIsosceles{
    private int numeroLados = 3;
    private float constante = (float)(Math.sqrt(2)/12);

    public double perimetro(double longitudLado){
        return numeroLados * longitudLado;
    }
    public double area(double base,double altura){
        return (base*altura)/2;
    }
    public double volumen(double base,double altura){
        return  (constante * (float) Math.pow(area(base,altura),3));
    }
}

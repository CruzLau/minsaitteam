import java.util.Dictionary;
import java.util.Hashtable;


public class PoligonoRegular{

    private String nombre;
    private int numeroLados=0;
    private double longitudLado=0;
    private double area;
    private double perimetro;
    private double apotema;
    private double anguloInterno;
    private double altura;
    private double volumen=0;
    private static Dictionary<Integer,String> nombresSugeridos;
    private static void inicializaNombresSugeridos(){
        nombresSugeridos=new Hashtable<Integer, String>();
        nombresSugeridos.put(5,"pent");
        nombresSugeridos.put(6,"hex");
        nombresSugeridos.put(7,"hept");
        nombresSugeridos.put(8,"oct");
        nombresSugeridos.put(9,"ene");
        nombresSugeridos.put(10,"dec");
        nombresSugeridos.put(11,"undec");
        nombresSugeridos.put(12,"dodec");
        nombresSugeridos.put(13,"tridec");
        nombresSugeridos.put(14,"tetradec");
    }
    private void sugiereNombre()
    {
        nombre= nombresSugeridos.get(getNumeroLados())+"ágono";
    }
    public PoligonoRegular(int numeroLados,double longitudLado){
        creaPolignoRegularSinAltura(numeroLados,longitudLado);
    }
    public PoligonoRegular(int numeroLados,double longitudLado,double altura){
        creaPolignoRegularSinAltura(numeroLados,longitudLado);
        setAltura(altura);
    }
    public void creaPolignoRegularSinAltura(int numeroLados,double longitudLado){
        inicializaNombresSugeridos();
        setNumeroLados(numeroLados);
        setLongitudLado(longitudLado);
    }

    public int getNumeroLados(){return numeroLados;}
    public String getNombre(){return nombre;}
    public double getLongitudLado(){return longitudLado;}
    public double getArea(){return area;}
    public double getApotema(){return apotema;}
    public double getPerimetro(){return perimetro;}
    public double getAltura(){return altura;}
    public double getAnguloInterno(){return anguloInterno;}
    public double getVolumen(){return volumen;}
    public void setNumeroLados(int numeroLados){
        checaNumeroDeLados(numeroLados);
        this.numeroLados=numeroLados;
        sugiereNombre();
        actualizaCalculos();
    }
    public void setLongitudLado(double longitudLado)
    {
        verificaNumeroPositivo(longitudLado);
        this.longitudLado=longitudLado;
        actualizaCalculos();
    }

    public void checaNumeroDeLados(int numeroLados)
    {
        if(numeroLados<5 || numeroLados>14)
        {
            System.out.println("Numeros de lados debe ser entre 4 y 14");
            System.exit(0);
        }
    }

    public void setAltura(double altura){
        verificaNumeroPositivo(altura);
        this.altura=altura;
        calculaVolumen();
    }

    private void calculaPerimetro()
    {
        perimetro=numeroLados*longitudLado;

    }

    private void calculaArea()
    {
        area=perimetro*apotema/2;
    }

    private void calculaVolumen(){
        volumen=area*altura;
    }

    private void calculaAnguloYApotema()
    {
        calculaAnguloInterno();
        calculaApotema();
    }

    private void calculaAnguloInterno()
    {
        anguloInterno=360/numeroLados;
    }

    private void calculaApotema()
    {
        apotema=longitudLado/(2*Math.tan(Math.toRadians(anguloInterno)));
    }
    private void actualizaPerimetroYArea()
    {
        calculaPerimetro();
        calculaArea();
    }

    private void actualizaCalculos()
    {
        calculaAnguloYApotema();
        actualizaPerimetroYArea();
        calculaVolumen();
    }

    private void verificaNumeroPositivo(double numero)
    {
        if(numero<0)
        {
            System.out.println("Longitud de lado o altura debe ser mayor a 0");
            System.exit(0);
        }
    }
}

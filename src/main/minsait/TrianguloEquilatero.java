
public class TrianguloEquilatero{
    private double lado;
    private double areaTriangulo;
    private double perimetroTriangulo;
    private double volumenTriangulo;

    public TrianguloEquilatero(double lado){
        this.lado = lado;
    }

    public double getLado(){
        return lado;
    }

    public void setLado(double lado){
        this.lado =lado;
    }

    public double getareaTriangulo(){
        return areaTriangulo;
    }
    public double getPerimetroTriangulo(){
        return perimetroTriangulo;
    }

    public double getvolumenTriangulo(){
        return volumenTriangulo;
    }

    public double area(){
        areaTriangulo = (Math.sqrt(3)*(lado*lado))/4;
        return areaTriangulo;
    }
    public double perimetro(){
        perimetroTriangulo= 3*lado;
        return perimetroTriangulo;
    }
    public double volumen()
    {
        return volumenTriangulo=areaTriangulo*lado;
    }


}

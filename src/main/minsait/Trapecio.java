public class Trapecio {

    public double baseMayor;
    public double baseMenor;
    public double altura;
    public double lado;
    public double area;
    public double perimetro;

    public Trapecio(double baseMayor, double baseMenor, double altura, double lado){
        this.baseMayor=baseMayor;
        this.baseMenor=baseMenor;
        this.altura=altura;
        this.lado = lado;
    }


    public double areaTrapecio(){
        area = ((baseMayor + baseMenor)/2) * altura;
        return area;
    }

    public double perimetroTrapecio(){
        perimetro = baseMayor +baseMenor+(lado*2);
        return perimetro;
    }


}

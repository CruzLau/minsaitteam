
public class Cuadrado
{
    private double area,volumen;

    public double calcularAreaCuadrado(double longitud)
    {
        area=(float)Math.pow(longitud, 2);
        return area;
    }

    public double calcularPerimetroCuadrado(double longitud)
    {
        return 4*longitud;
    }

    public double calcularVolumenCuadrado(double longitud)
    {
        volumen=(float) Math.pow(area, 3);
        return volumen;
    }


}
